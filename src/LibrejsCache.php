<?php

namespace Drupal\librejs;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\DatabaseExceptionWrapper;

/**
 * Decorates the asset resolver cache so cache set calls can be intercepted.
 */
class LibrejsCache implements CacheBackendInterface {

  /**
   * Aggregated JS license.
   */
  protected License $license = License::GplV2;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    protected CacheBackendInterface $cache,
    protected CacheTagsInvalidatorInterface $invalidator,
    protected Connection $database,
  ) {
  }

  /**
   * Ensures that aggregated JS uses a compatible license.
   */
  public function setCompatibleLicense(string $identifier): void {
    $this->license = $this->license->getCompatibleLicense($identifier);
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-ignore missingType.parameter,missingType.parameter
   */
  public function get($cid, $allow_invalid = FALSE) {
    return $this->cache->get($cid, $allow_invalid);
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-ignore missingType.parameter
   */
  public function getMultiple(&$cids, $allow_invalid = FALSE) {
    return $this->cache->getMultiple($cids, $allow_invalid);
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-ignore missingType.iterableValue,missingType.return
   */
  public function set($cid, $data, $expire = Cache::PERMANENT, array $tags = []) {
    $upserts = [];
    if (str_starts_with($cid, 'js:') && is_array($data) && isset($data[0]) && is_array($data[0]) && isset($data[1]) && is_array($data[1])) {
      foreach ([0, 1] as $key) {
        foreach ($data[$key] as $js) {
          if (!empty($js['preprocessed'])) {
            if ($path = parse_url($js['data'], PHP_URL_PATH)) {
              if (str_starts_with($path, base_path())) {
                $path = substr($path, strlen(base_path()));
              }
              $upserts[$path] = [
                'data' => $path,
                'version' => NULL,
                'type' => 'preprocessed',
                // Preprocessing is disabled for non-GPL-compatible JS.
                'license' => $this->license->value,
                'url' => $this->license->getUrl(),
                'source' => NULL,
                'query' => parse_url($js['data'], PHP_URL_QUERY),
              ];
            }
          }
        }
      }
    }
    if ($upserts) {
      $upsert = $this->database->upsert('librejs')
        ->fields(['data', 'version', 'type', 'license', 'url', 'source', 'query'])
        ->key('data');
      foreach ($upserts as $values) {
        $upsert->values($values);
      }
      try {
        $upsert->execute();
      }
      catch (DatabaseExceptionWrapper $e) {
        // Database migration may be pending.
      }
      $this->invalidator->invalidateTags(['librejs_jslicense']);
    }
    return $this->cache->set($cid, $data, $expire, $tags);
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-ignore missingType.iterableValue,missingType.return
   */
  public function setMultiple(array $items) {
    return $this->cache->setMultiple($items);
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-ignore missingType.return
   */
  public function delete($cid) {
    return $this->cache->delete($cid);
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-ignore missingType.iterableValue,missingType.return
   */
  public function deleteMultiple(array $cids) {
    return $this->cache->deleteMultiple($cids);
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-ignore missingType.return
   */
  public function deleteAll() {
    return $this->cache->deleteAll();
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-ignore missingType.return
   */
  public function invalidate($cid) {
    return $this->cache->invalidate($cid);
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-ignore missingType.return
   */
  public function invalidateMultiple(array $cids) {
    return $this->cache->invalidateMultiple($cids);
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-ignore missingType.return
   */
  public function invalidateAll() {
    return $this->cache->invalidateAll();
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-ignore missingType.return
   */
  public function garbageCollection() {
    return $this->cache->garbageCollection();
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-ignore missingType.return
   */
  public function removeBin() {
    return $this->cache->removeBin();
  }

}
