<?php

namespace Drupal\librejs;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Decorates the asset resolver cache so cache set calls can be intercepted.
 */
class LibrejsServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container): void {
    if (!$container->hasDefinition('asset.resolver')) {
      return;
    }
    $container->getDefinition('asset.resolver')->replaceArgument(5, new Reference('librejs.cache'));
  }

}
