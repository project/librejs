<?php

namespace Drupal\librejs\Hook;

use Drupal\Core\Asset\AttachedAssetsInterface;
use Drupal\Core\Asset\LibraryDependencyResolverInterface;
use Drupal\Core\Asset\LibraryDiscoveryInterface;
use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Hook\Attribute\Hook;
use Drupal\librejs\LibrejsCache;

/**
 * Implements hook_js_alter().
 */
#[Hook('js_alter')]
class JsAlter {

  public function __construct(
    protected CacheTagsInvalidatorInterface $cacheTagsInvalidator,
    protected Connection $connection,
    protected LibraryDependencyResolverInterface $libraryDependencyResolver,
    protected LibraryDiscoveryInterface $libraryDiscovery,
    protected LibrejsCache $librejsCache,
  ) {
  }

  /**
   * Implements hook_js_alter().
   *
   * @param array<array<scalar|mixed[]>> $javascript
   *   An array of all JavaScript being presented on the page.
   * @param \Drupal\Core\Asset\AttachedAssetsInterface $assets
   *   The assets attached to the current response.
   */
  public function __invoke(array &$javascript, AttachedAssetsInterface $assets): void {
    $statement = $this->connection
      ->select('librejs')
      ->fields('librejs', ['data', 'version'])
      ->execute();
    if (NULL === $statement) {
      throw new \UnexpectedValueException('Database query failed.');
    }
    $librejs = $statement->fetchAllAssoc('data');
    foreach ($javascript as $key => &$js) {
      // Ensure aggregated JavaScript uses a compatible license.
      if (isset($js['license']['name']) && is_string($js['license']['name'])) {
        $this->librejsCache->setCompatibleLicense($js['license']['name']);
      }
      // Do not aggregate non-GPL-compatible JS so we can consider aggregated JS
      // to be GPL-compatible.
      if (empty($js['license']['gpl-compatible'])) {
        $js['preprocess'] = FALSE;
      }
      if (!is_string($js['data'])) {
        continue;
      }
      if (!isset($librejs[$js['data']]) || $librejs[$js['data']]->version != $js['version']) {
        $upserts[$key] = [
          'data' => $js['data'],
          'version' => $js['version'],
          'type' => $js['type'],
          'license' => $js['license']['name'] ?? NULL,
          'url' => $js['license']['url'] ?? NULL,
        ];
      }
    }
    if (empty($upserts)) {
      return;
    }
    // Fetch source URL for each attached library.
    $libraries = $this->libraryDependencyResolver->getLibrariesWithDependencies($assets->getLibraries());
    foreach ($libraries as $library) {
      [$extension, $name] = explode('/', $library, 2);
      $definition = $this->libraryDiscovery->getLibraryByName($extension, $name);
      if (isset($definition['js'])) {
        foreach ($definition['js'] as $options) {
          if (isset($upserts[$options['data']])) {
            $upserts[$options['data']]['source'] = $definition['remote'] ?? NULL;
          }
        }
      }
    }
    $upsert = $this->connection->upsert('librejs')
      ->fields(['data', 'version', 'type', 'license', 'url', 'source'])
      ->key('data');
    foreach ($upserts as $values) {
      $upsert->values($values);
    }
    $upsert->execute();
    $this->cacheTagsInvalidator->invalidateTags(['librejs_jslicense']);
  }

}
