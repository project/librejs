<?php

namespace Drupal\librejs\Hook;

use Drupal\Core\Database\Connection;
use Drupal\Core\Hook\Attribute\Hook;

/**
 * Implements hook_cache_flush().
 */
#[Hook('cache_flush')]
class CacheFlush {

  public function __construct(protected Connection $connection) {
  }

  /**
   * Implements hook_cache_flush().
   */
  public function __invoke(): void {
    $this->connection->truncate('librejs')->execute();
  }

}
