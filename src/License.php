<?php

namespace Drupal\librejs;

/**
 * Provides the set of valid licenses for aggregated JS.
 */
enum License: string {

  case GplV2 = 'GPL-2.0-or-later';
  case GplV3 = 'GPL-3.0-or-later';

  /**
   * Returns license URL.
   */
  public function getUrl(): string {
    return match ($this) {
      self::GplV2 => 'http://www.gnu.org/licenses/gpl-2.0.html',
      self::GplV3 => 'http://www.gnu.org/licenses/gpl-3.0.html',
    };
  }

  /**
   * Returns GPLv3 if given license is compatible with GPLv3 but not GPLv2.
   */
  public function getCompatibleLicense(string $identifier): self {
    return match ($identifier) {
      'AGPL-3.0', 'AGPL-3.0-only', 'AGPL-3.0-or-later', 'Apache-2.0', 'GPL-3.0', 'GPL-3.0-only', 'GPL-3.0-or-later', 'LGPL-3.0', 'LGPL-3.0-only', 'LGPL-3.0-or-later' => self::GplV3,
      default => $this,
    };
  }

}
