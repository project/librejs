<?php

namespace Drupal\librejs\Controller;

use Drupal\Core\Asset\AssetQueryStringInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Query\TableSortExtender;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;

/**
 * Returns responses for LibreJS routes.
 */
class LibrejsController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('database'),
      // @phpstan-ignore ternary.alwaysTrue
      $container->has('asset.query_string') ? $container->get('asset.query_string') : NULL,
    );
  }

  /**
   * Constructs a LibrejsController object.
   */
  final public function __construct(
    protected Connection $database,
    protected ?AssetQueryStringInterface $assetQueryString,
  ) {
  }

  /**
   * Builds the response.
   *
   * @return mixed[]
   *   The renderable page.
   */
  public function build(): array {

    $build['content'] = [
      '#type' => 'table',
      '#header' => [
        [
          'data' => $this->t('Script'),
          'field' => 'librejs.data',
          'sort' => 'asc',
        ],
        [
          'data' => $this->t('License'),
          'field' => 'librejs.license',
          'class' => [RESPONSIVE_PRIORITY_MEDIUM],
        ],
        [
          'data' => $this->t('Source'),
          'field' => 'librejs.source',
          'class' => [RESPONSIVE_PRIORITY_LOW],
        ],
      ],
      '#attributes' => ['id' => 'jslicense-labels1'],
      '#cache' => [
        'contexts' => ['url.query_args:sort', 'url.query_args:order'],
        'tags' => ['librejs_jslicense'],
      ],
    ];

    $query = $this->database->select('librejs', 'librejs');
    // @fixme Try/catch for multiple versions of core.
    try {
      // @fixme Drupal 9.3 compatibility.
      $query = $query->extend(TableSortExtender::class);
    }
    catch (ServiceNotFoundException $e) {
      $query = $query->extend('table_sort');
    }
    // @fixme Static analysis wants these method_exists() calls.
    if (method_exists($query, 'fields')) {
      $query->fields('librejs', [
        'data',
        'license',
        'url',
        'source',
        'version',
        'type',
        'query',
      ]);
    }
    if (method_exists($query, 'orderByHeader')) {
      $query->orderByHeader($build['content']['#header']);
    }
    if (method_exists($query, 'execute')) {
      $javascript = $query->execute();
      foreach ($javascript as $js) {
        if ($js->type == 'external') {
          $url = Url::fromUri($js->data);
          $text = $url->toString();
        }
        else {
          // Preprocessed (aggregated) assets will already have a query string.
          if ($js->type === 'preprocessed') {
            $query = [];
            parse_str($js->query, $query);
            $url = Url::fromUri("base:{$js->data}", ['query' => $query]);
            $text = basename($js->data);
          }
          else {
            if ($js->version == -1) {
              $query = [$this->assetQueryString ? $this->assetQueryString->get() : $this->state()->get('system.css_js_query_string', '0') => NULL];
            }
            else {
              $query = ['v' => $js->version];
            }
            $url = Url::fromUri("base:{$js->data}", ['query' => $query]);
            $text = $js->data;
          }
        }
        $build['content']['#rows'][] = [
          [
            'data' => Link::fromTextAndUrl($text, $url),
            'style' => 'overflow-wrap: anywhere',
          ],
          static::createLinkFromLicenseAndUrl($js->license, $js->url),
          [
            'data' => $js->source ? Link::fromTextAndUrl($js->source, Url::fromUri($js->source)) : Link::fromTextAndUrl($js->type === 'preprocessed' ? $this->t('Aggregated') : basename($js->data), $url),
            'style' => 'overflow-wrap: anywhere',
          ],
        ];
      }
    }
    return $build;
  }

  /**
   * Creates a link from license identifier and license URL.
   *
   * @param string $license
   *   The license identifier.
   * @param string $url
   *   The license URL.
   */
  public static function createLinkFromLicenseAndUrl($license, $url): Link {
    // Rewrite some license identifiers for compatibility with LibreJS browser
    // extension.
    $license = match ($license) {
      'GNU-GPL-2.0-or-later' => 'GPL-2.0-or-later',
      'MIT' => 'Expat',
      default => $license,
    };
    return Link::fromTextAndUrl($license, Url::fromUri($url));
  }

}
