<?php

namespace Drupal\Tests\librejs\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests LibreJS module.
 *
 * @group librejs
 */
class LibrejsTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to install.
   *
   * @var string[]
   */
  protected static $modules = ['librejs', 'librejs_test'];

  /**
   * Tests LibreJS module.
   */
  public function testLibrejs(): void {
    $admin_user = $this->drupalCreateUser([
      'access jslicense',
    ]);
    $this->assertNotFalse($admin_user);
    $this->drupalLogin($admin_user);
    $this->drupalGet('librejs/jslicense');
    $this->assertSession()->pageTextContains('core/misc/drupal.js');
    $this->assertSession()->pageTextContains('GPL-2.0-or-later');
  }

}
