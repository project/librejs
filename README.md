# LibreJS module

LibreJS module allows site administrators and users to view the license of each
JavaScript file used by the site at /librejs/jslicense.

LibreJS module will only discover JavaScript files as they are loaded, so you
will have to visit this page repeatedly to view JavaScript files and URLs as
they are detected.

Only roles granted the "access JavaScript license information" permission can
access the JavaScript license information page. Warning: This permission allows
users to enumerate and read all JavaScript files used by the site.

For compatibility with the LibreJS browser extension, LibreJS module disables
aggregation of non-GPL-compatible JavaScript. This allows GPL-compatible
aggregated JS to be accepted by LibreJS browser extension. In addition, LibreJS
module rewrites some license identifiers to use those preferred by LibreJS
browser extension.
